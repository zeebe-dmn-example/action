package net.pervukhin.action.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ActionService {
    private static final Logger logger = LoggerFactory.getLogger(ActionService.class);

    public void testAction(String statusType, Map<String, Object> variables) {
        logger.info("Event Logged with statusType {}", statusType);
        variables.entrySet().forEach(item -> logger.info("Variable {} = {}", item.getKey(), item.getValue()));
    }
}
