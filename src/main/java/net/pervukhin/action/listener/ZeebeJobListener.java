package net.pervukhin.action.listener;

import io.zeebe.client.ZeebeClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import net.pervukhin.action.service.ActionService;

import javax.annotation.PostConstruct;

@Service
public class ZeebeJobListener {
    private static final Logger logger = LoggerFactory.getLogger(ZeebeJobListener.class);
    private final ZeebeClient zeebeClient;
    private final String jobWorker;
    private final ActionService actionService;
    private static final String STATUS_TYPE_FIELD = "statusType";

    @Autowired
    public ZeebeJobListener(ZeebeClient zeebeClient,
                            ActionService actionService,
                            @Value("${zeebe-client.worker-name}") String jobWorker) {
        this.zeebeClient = zeebeClient;
        this.jobWorker = jobWorker;
        this.actionService = actionService;
    }

    @PostConstruct
    public void init() {
        logger.debug("Zeebe subscription");
        subscribe();
    }

    private void subscribe() {
        zeebeClient.newWorker().jobType(String.valueOf(jobWorker)).handler(
                (jobClient, activatedJob) -> {
                    logger.debug("Received message from Workflow");
                    actionService.testAction(
                            activatedJob.getCustomHeaders().get(STATUS_TYPE_FIELD),
                            activatedJob.getVariablesAsMap());
                    jobClient.newCompleteCommand(activatedJob.getKey())
                            .send()
                            .join();
                }
        ).open();
    }
}
